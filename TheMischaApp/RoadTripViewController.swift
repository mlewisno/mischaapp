import UIKit
import QuartzCore

class RoadTripViewController: UIViewController, UIScrollViewDelegate {
 
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tripSegmentedView: UISegmentedControl!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    
    var currentlySelectedImageIndex: Int = 0
    
    var imageCount = 0
    var imageSets = RoadTripImageManager.roadTripManagerInstance.imageSets
    
    var previousImage: UIImage?
    var currentImage = UIImage()
    var nextImage: UIImage?
    
    let imageView = UIImageView()
        
    @IBAction func onSectionSelection(sender: UISegmentedControl) {
        let selectedTitle = sender.titleForSegmentAtIndex(sender.selectedSegmentIndex)
        
        let selectedImageIndex = RoadTripImageManager.getIndexForFirstImageInSelectedSection(selectedTitle!)
        
        let selectedImageCoordinate: CGPoint = CGPoint(x: CGFloat(selectedImageIndex) * self.view.frame.width, y: 0)
    }
    
    @IBAction func onNextButtonTap(sender: UIButton) {
        currentlySelectedImageIndex++
        previousButton.hidden = false
        
        if currentlySelectedImageIndex == imageCount - 1 {
            nextButton.hidden = true
        }
        
        UIView.animateWithDuration(0.325, delay: 0, options: nil, animations: {
            self.imageView.alpha = 0.0
            }, completion: { done in
                self.animateFadeInUp()
        })
        
    }
    
    @IBAction func onPreviousButtonTap(sender: UIButton) {
        currentlySelectedImageIndex--
        nextButton.hidden = false
        
        if currentlySelectedImageIndex == 0 {
            previousButton.hidden = true
        }
        
        UIView.animateWithDuration(0.325, delay: 0, options: nil, animations: {
            self.imageView.alpha = 0.0
            }, completion: { done in
                self.animateFadeInDown()
        })
    }
    
    @IBAction func onBackButtonTap(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func getImage(imageIndex: Int) -> UIImage {
        return UIImage(named: RoadTripImageManager.roadTripImageNameWithGeneralIndex(imageIndex))!
    }
        
    func animateFadeInUp(){
        previousImage = currentImage
        if (nextImage != nil){
            currentImage = nextImage!
            imageView.image = currentImage
        }
        nextImage = UIImage(named: RoadTripImageManager.roadTripImageNameWithGeneralIndex(currentlySelectedImageIndex + 1))
        UIView.animateWithDuration(0.325, delay: 0, options: nil, animations: {
            self.imageView.alpha = 1.0
            },
            completion: nil
        )
    }
    
    func animateFadeInDown(){
        nextImage = currentImage
        if (previousImage != nil){
            currentImage = previousImage!
            imageView.image = currentImage
        }
        previousImage = UIImage(named: RoadTripImageManager.roadTripImageNameWithGeneralIndex(currentlySelectedImageIndex - 1))
        UIView.animateWithDuration(0.325, delay: 0, options: nil, animations: {
            self.imageView.alpha = 1.0
            },
            completion: nil
        )
    }
    
    override func viewDidLoad() {
        previousButton.hidden = true
        
        imageView.frame = CGRectMake(CGFloat(0), CGFloat(0), self.view.frame.width, self.view.frame.height)
        imageView.contentMode = UIViewContentMode.Center
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        
        currentImage = getImage(0)
        nextImage = getImage(1)
        imageView.image = currentImage
        
        for (var i = 0; i < imageSets.count; i++) {
            for (location, number) in imageSets[i] {
                imageCount += number
            }
        }
                
        self.view.addSubview(imageView)
    }
    
}
