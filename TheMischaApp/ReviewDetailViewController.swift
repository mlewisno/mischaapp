import UIKit

class ReviewDetailViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tldrLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var reviewTextView: UITextView!
    
    var review: Review?
    
    @IBAction func onBackButtonTap(sender: UIBarButtonItem) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = review!.image
        nameLabel.text = review!.name
        tldrLabel.text = review!.tldr
        reviewTextView.text = review!.review
        descriptionLabel.text = review!.descriptor
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
