import UIKit
import QuartzCore

class CategoryActionView: UIView {
    
    var labelText = ""
    var categoryIconImageName = ""
    
    let categoryLabel = UILabel()
    let categoryIconView = UIImageView()
    let scrollView = UIScrollView()
    
    override func drawRect(rect: CGRect)
    {
        super.drawRect(rect)
        
        var width = super.frame.width
        var height = super.frame.height
        
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        
        scrollView.frame = CGRectMake(CGFloat(0), CGFloat(0), super.frame.width, super.frame.height)
        
        categoryLabel.text = labelText
        categoryLabel.textColor = UIColor.whiteColor()
        categoryLabel.textAlignment = .Center
        categoryLabel.font = UIFont.systemFontOfSize(CGFloat(23))
        categoryLabel.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.65)
        
        categoryIconView.image = UIImage(named: categoryIconImageName)
        categoryIconView.contentMode = UIViewContentMode.Center
        categoryIconView.contentMode = UIViewContentMode.ScaleAspectFill
        
        
        categoryLabel.clipsToBounds = true
        categoryIconView.clipsToBounds = false
        
        scrollView.addSubview(categoryIconView)
        scrollView.addSubview(categoryLabel)
        
        self.addSubview(scrollView)
        
        scrollContentIn(super.frame.width, height: super.frame.height)
        
        reload()
        
    }
    
    func scrollContentIn(width: CGFloat, height: CGFloat){
        
        categoryLabel.frame = CGRectMake(0, 3 * height/4, width * 3, height/4)
        categoryIconView.frame = CGRect(x: 4 * width / 3, y: height / 6, width: width / 3, height: height / 3)
        
        self.scrollView.contentOffset = CGPoint(x: 0, y: 0)
        
        UIView.animateWithDuration(1.5, delay: 2, options: nil, animations: {
            self.scrollView.contentOffset = CGPoint(x: width, y: 0)
            }, completion: nil
        )
    }
    
    func setAttibutes(label: String, iconName: String){
        
        self.labelText = "About"
        self.categoryIconImageName = "aboutMe"
        
    }
    
    func reload(){
    }
    
}
