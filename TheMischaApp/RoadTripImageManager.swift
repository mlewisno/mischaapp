import Foundation
import UIKit

private let sharedRoadTripManagerInstance = RoadTripImageManager()

class RoadTripImageManager {
    
    let imageSets = [
        ["Colorado" : 18],
        ["New Mexico" : 7],
        ["Arizona" : 19]
    ]
    
    class var roadTripManagerInstance: RoadTripImageManager {
        return sharedRoadTripManagerInstance
    }
    
    class func roadTripImageNameWithGeneralIndex(imageIndex: Int) -> String {
        
        var index = imageIndex
        
        for (var i = 0; i < roadTripManagerInstance.imageSets.count; i++) {
            for (location, number) in roadTripManagerInstance.imageSets[i] {
                if index < number {
                    return "\(location)-\(index)"
                }
                else {
                    index -= number
                }
            }
        }
        return ""
    }
    
    class func getIndexForFirstImageInSelectedSection(name: String) -> Int {
        
        var imageCount = 0
        
        for (var i = 0; i < roadTripManagerInstance.imageSets.count; i++) {
            for (location, number) in roadTripManagerInstance.imageSets[i] {
                if name == location {
                    return imageCount
                }
                else {
                    imageCount += number
                }
            }
        }
        return 0
    }
    
    init() {
    }
    
}
