import UIKit
import QuartzCore

class AboutView: CategoryActionView {
    
    override func drawRect(rect: CGRect)
    {
        super.setAttibutes("About", iconName: "aboutMe")
        super.drawRect(rect)
    }
}
