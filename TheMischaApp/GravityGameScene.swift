import SpriteKit

let kParticleCategory: UInt32 = 1 << 0
let kBoundariesCategory: UInt32 = 1 << 1

class GravityGameScene: SKScene, SKPhysicsContactDelegate {
    
    var lastUpdateTimeInterval: CFTimeInterval = 1
    var currentBackground: SKSpriteNode!
    
    override func didMoveToView(view: SKView) {
        self.physicsWorld.contactDelegate = self
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        
        self.currentBackground = createBackground()
        addChild(currentBackground)
        
        let secondBackground = createBackground()
        secondBackground.position = CGPoint(x: currentBackground.size.width, y: 0)
        addChild(secondBackground)
        
        // level boundaries
        let leftCollider = SKNode()
        leftCollider.position = CGPoint(x: 0, y: 0)
        leftCollider.physicsBody = SKPhysicsBody(edgeFromPoint: CGPointZero, toPoint: CGPoint(x: 0, y: size.height))
        leftCollider.physicsBody!.categoryBitMask = kBoundariesCategory
        leftCollider.physicsBody!.restitution = 1.0
        addChild(leftCollider)
        
        let rightCollider = SKNode()
        rightCollider.position = CGPoint(x: size.width, y: 0)
        rightCollider.physicsBody = SKPhysicsBody(edgeFromPoint: CGPointZero, toPoint: CGPoint(x: 0, y: size.height))
        rightCollider.physicsBody!.categoryBitMask = kBoundariesCategory
        rightCollider.physicsBody!.restitution = 1.0
        addChild(rightCollider)
        
        let bottomCollider = SKNode()
        bottomCollider.position = CGPoint(x: 0, y: 0)
        bottomCollider.physicsBody = SKPhysicsBody(edgeFromPoint: CGPointZero, toPoint: CGPoint(x: size.width, y: 0))
        bottomCollider.physicsBody!.categoryBitMask = kBoundariesCategory
        bottomCollider.physicsBody!.restitution = 1.0
        addChild(bottomCollider)
        
        let topCollider = SKNode()
        topCollider.position = CGPoint(x: 0, y: size.height)
        topCollider.physicsBody = SKPhysicsBody(edgeFromPoint: CGPointZero, toPoint: CGPoint(x: size.width, y: 0))
        topCollider.physicsBody!.categoryBitMask = kBoundariesCategory
        topCollider.physicsBody!.restitution = 1.0
        addChild(topCollider)
        
        spawnParticles()
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        var leftTouch = false
        for touch in touches {
            if touch.locationInView(self.view).x < self.size.width / 2.0 && touch.phase != .Ended {
                leftTouch = true
            }
        }
        /*
        if playerMovingUp && !leftTouch {
            playerNode.physicsBody!.velocity = CGVector(dx: 0, dy: 0)
            playerMovingUp = false
        }
        */
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        var leftTouch = false
        var rightTouch = false
        for touch in touches {
            if touch.locationInView(self.view).x < self.size.width / 2.0 {
                leftTouch = true
            }
            else {
                rightTouch = true
            }
        }
        /*
        if !playerMovingUp && leftTouch {
            playerNode.physicsBody!.velocity = CGVector(dx: 0, dy: 0)
            playerMovingUp = true
        }
        
        if rightTouch {
            shootLaser()
        }
        */
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        var ellapsedTime = currentTime - lastUpdateTimeInterval
        
        if (ellapsedTime > 1){
            ellapsedTime = 1.0 / 60.0;
        }
        
        updateBackground(ellapsedTime)
        
    }
    
    func createBackground() -> SKSpriteNode {
        let background = SKSpriteNode(imageNamed: "SpaceBackground")
        background.name = "background"
        background.anchorPoint = CGPoint(x:0, y:0)
        background.position = CGPoint(x:0, y:0)
        
        return background
    }
    
    func updateBackground(ellapsedTime: CFTimeInterval){
        
        enumerateChildNodesWithName("background", usingBlock: { node, stop in
            
            node.position = CGPoint(x: node.position.x - 60 * CGFloat(ellapsedTime), y: node.position.y)
            
            if node != self.currentBackground && self.currentBackground.position.x <= -self.currentBackground.frame.size.width {
                self.currentBackground.position = CGPointMake(node.position.x + node.frame.size.width, 0);
                self.currentBackground = node as SKSpriteNode
            }

        })
        
    }
    
    func spawnParticles() {
        
        for i in 0...5 {
            let particleXCoordinate = CGFloat(drand48()) * self.size.width
            let particleYCoordinate = CGFloat(drand48()) * self.size.height
            
            let particle = SKSpriteNode(imageNamed: "Ball")
            particle.name = "particle"
            particle.xScale = 0.2
            particle.yScale = particle.xScale
            particle.physicsBody = SKPhysicsBody(circleOfRadius: particle.size.width / 2.0)
            particle.position = CGPoint(x: particleXCoordinate, y: particleYCoordinate)
            particle.physicsBody!.collisionBitMask = kParticleCategory | kBoundariesCategory
            particle.physicsBody!.categoryBitMask = kParticleCategory
            particle.physicsBody!.angularDamping = 0
            particle.physicsBody!.density = 5.0
            addChild(particle)
            
            particle.physicsBody!.applyImpulse(CGVector(dx: -300*CGFloat(drand48())-150, dy: -300*CGFloat(drand48())-150))
            particle.physicsBody!.applyAngularImpulse(-0.1 + CGFloat(drand48()) * 0.2)
        }
        
        
        
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody!
        var secondBody: SKPhysicsBody!
        
        // we assign the node with the smaller mask value to firstBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
    }
}
