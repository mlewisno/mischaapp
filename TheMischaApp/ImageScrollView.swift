import UIKit

class ImageScrollView: UIView {

    let fadingImageView = UIImageView(image: UIImage(named: "New Mexico-6"))
    let roadTripImageManager = RoadTripImageManager.roadTripManagerInstance
    let natureImageManager = NatureImageManager.natureManagerInstance
    
    override func drawRect(rect: CGRect)
    {
        super.drawRect(rect)
        
        fadingImageView.frame = CGRectMake(CGFloat(0), CGFloat(0), super.frame.width, super.frame.height)
        fadingImageView.contentMode = UIViewContentMode.Center
        fadingImageView.contentMode = UIViewContentMode.ScaleAspectFill
        fadingImageView.clipsToBounds = true
        
        self.addSubview(fadingImageView)
        
        animateFadeOut()
        
    }
    
    func animateFadeOut(){
        UIView.animateWithDuration(0.75, delay: 6, options: nil, animations: {
            self.fadingImageView.alpha = 0.0
            }, completion: { done in
            self.animateFadeIn()
        })
    }
    
    func animateFadeIn(){
        let coinFlip = arc4random_uniform(2)
        if coinFlip == 0 {
            let imageIndex = arc4random_uniform(21)
            fadingImageView.image = UIImage(named: NatureImageManager.natureImageNameWithGeneralIndex(Int(coinFlip)))
        }
        else {
            let imageIndex = arc4random_uniform(16)
            fadingImageView.image = UIImage(named: NatureImageManager.natureImageNameWithGeneralIndex(Int(coinFlip)))
        }
        UIView.animateWithDuration(0.75, delay: 0, options: nil, animations: {
            self.fadingImageView.alpha = 1.0
            }, completion: { done in
                self.animateFadeOut()
        })
    }
    
    

}
