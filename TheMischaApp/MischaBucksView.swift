import UIKit

class MischaBucksView: CategoryActionView {
    
    override func drawRect(rect: CGRect)
    {
        super.setAttibutes("Mischa Bucks", iconName: "mischaBucks")
        super.drawRect(rect)
    }
    
    override func reload(){
        let current = NSUserDefaults.standardUserDefaults().valueForKey("numberOfMischaBucks") as Int
        categoryLabel.text = "\(current) Mischa Bucks!"
    }
    
}