import Foundation
import UIKit

class Review {
    
    enum gameType {
        case VideoGame
        case BoardGame
        case CardGame
        case SocialGame
        case MiscGame
    }
    
    var name: String
    var type: gameType
    var descriptor: String
    var review: String
    var tldr: String
    var image: UIImage
    
    init(name: String, type: gameType, descriptor: String, review: String, tldr: String, image: UIImage){
        self.name = name
        self.type = type
        self.descriptor = descriptor
        self.review = review
        self.tldr = tldr
        self.image = image
    }
}