import Foundation
import UIKit

private let sharedMischaManagerInstance = MischaTripImageManager()

class MischaTripImageManager {
    
    let numberOfImages = 1
    
    class var mischaManagerInstance: MischaTripImageManager {
        return sharedMischaManagerInstance
    }
    
    class func mischaImageName(imageIndex: Int) -> String {
        return "Mischa-\(imageIndex)"
    }
    
    init() {
    }
    
}