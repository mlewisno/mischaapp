import UIKit

class AboutMeViewController: UIViewController {
   
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var aboutMeTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBOutlet weak var skillsLabel: UILabel!
    @IBOutlet weak var homeWorldLabel: UILabel!

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favoriteFoodLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
}
