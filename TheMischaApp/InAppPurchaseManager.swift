import UIKit
import StoreKit

private let sharedInAppPurchaseManager = InAppPurchaseManager()

let kInAppMischaBucksProductId = "com.mlewisno.TheMischaApp.MischaBucks"

class InAppPurchaseManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    var mischaBucks: SKProduct?
    var mischaBucksRequest: SKProductsRequest?
    
    class var inAppPurchaseManager: InAppPurchaseManager {
        return sharedInAppPurchaseManager
    }
    
    func requestMischaBucks(){
        var productIdentifiers: NSSet = NSSet(object: kInAppMischaBucksProductId)
        mischaBucksRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        mischaBucksRequest!.delegate = self
        mischaBucksRequest!.start()
    }
    
    func productsRequest(request: SKProductsRequest!, didReceiveResponse response: SKProductsResponse!) {
        var products = response.products
        if (products.count == 1){
            mischaBucks = products.first as? SKProduct
            
            if (mischaBucks != nil){
                println("Product Title: \(mischaBucks!.localizedTitle)")
                println("Product Description: \(mischaBucks!.localizedDescription)")
                println("Product Price: \(mischaBucks!.price)")
                println("Product ID: \(mischaBucks!.productIdentifier)")
            }
            
        }
        
        for invalidProductID in response.invalidProductIdentifiers {
            println("Invalid Product ID: \(invalidProductID)")
        }
        
    }
    
    func loadStore(){
        SKPaymentQueue.defaultQueue().addTransactionObserver(self)
        
        self.requestMischaBucks()
    }
    
    func canMakePurchases()->Bool{
        return SKPaymentQueue.canMakePayments()
    }
    
    func purchaseMischaBucks(){
        if (mischaBucks != nil){
            let payment: SKPayment = SKPayment(product: mischaBucks)
            SKPaymentQueue.defaultQueue().addPayment(payment)
        }
    }
    
    func recordTransaction(transaction: SKPaymentTransaction){
        
        if transaction.payment.productIdentifier == kInAppMischaBucksProductId {
            NSUserDefaults.standardUserDefaults().setValue(transaction.transactionIdentifier, forKey: "receipt")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
        
    }
    
    func provideContent(productID: String){
        if productID == kInAppMischaBucksProductId {
            let oldBucks = NSUserDefaults.standardUserDefaults().valueForKey("numberOfMischaBucks") as Int
            NSUserDefaults.standardUserDefaults().setValue(oldBucks + 1, forKey: "numberOfMischaBucks")
            NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    func finishTransaction(transaction: SKPaymentTransaction, wasSuccessful: Bool){
        SKPaymentQueue.defaultQueue().finishTransaction(transaction)
        
        let userInfo: NSDictionary = NSDictionary(dictionary: ["transaction": transaction])
        
        if (wasSuccessful){
            println("Transaction completed successfully")
        }
        else {
            println("Transaction did not complete successfully")
        }
        
    }
    
    func completeTransaction(transaction: SKPaymentTransaction){
        self.recordTransaction(transaction)
        self.provideContent(transaction.payment.productIdentifier)
        self.finishTransaction(transaction, wasSuccessful: true)
    }
    
    func restoreTransaction(transaction: SKPaymentTransaction){
        self.recordTransaction(transaction.originalTransaction)
        self.provideContent(transaction.originalTransaction.payment.productIdentifier)
        self.finishTransaction(transaction, wasSuccessful: true)
    }
    
    func failedTransaction(transaction: SKPaymentTransaction){
        
        if (transaction.error.code != SKErrorPaymentCancelled)
        {
            // error!
            self.finishTransaction(transaction, wasSuccessful: false)
        }
        else
        {
            // this is fine, the user just cancelled, so don’t notify
            SKPaymentQueue.defaultQueue().finishTransaction(transaction)
        }
    }
    
    
    func paymentQueue(queue: SKPaymentQueue!, updatedTransactions transactions: [AnyObject]!){
        
        for transaction in transactions
        {
            let castedTransaction = transaction as SKPaymentTransaction
            switch (castedTransaction.transactionState)
            {
            case SKPaymentTransactionState.Purchased:
                self.completeTransaction(castedTransaction)
                break
            case SKPaymentTransactionState.Failed:
                self.failedTransaction(castedTransaction)
                break
            case SKPaymentTransactionState.Restored:
                self.restoreTransaction(castedTransaction)
                break
            default:
                break
            }
        }
        
    }
    
}
