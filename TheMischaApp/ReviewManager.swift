import Foundation
import UIKit

private let reviewManagerInstance = ReviewManager()

class ReviewManager {
    
    var reviews = [Review]()
    
    class var sharedReviewManager: ReviewManager {
        return reviewManagerInstance
    }
    
    init() {
        reviews.append(
            Review(
                name: "Halo: Combat Evolved",
                type: Review.gameType.VideoGame,
                descriptor: "Sci-Fi First Person Shooter",
                review: "Here are some quick thoughts on Halo...",
                tldr: "A timeless classic with a fantastic sense of wonder",
                image: UIImage(named: "Halo Combat Evolved")!))
        
            reviews.append(
                Review(
                    name: "Settlers of Catan",
                    type: Review.gameType.BoardGame,
                    descriptor: "Resource Management Game",
                    review: "Here are some quick thoughts on Settlers of Catan...",
                    tldr: "A fantastic resource management game assuming you don't mind hating your friends at the end",
                    image: UIImage(named: "Settlers of Catan")!
                )
            )
        
            reviews.append(
                Review(
                    name: "Race For The Galaxy",
                    type: Review.gameType.CardGame,
                    descriptor: "Tableu Building Game",
                    review: "Here are some quick thoughts on Race For The Galaxy...",
                    tldr: "A great casual tableu building game that still has an excellent amount of strategy and depth",
                    image: UIImage(named: "Race For The Galaxy")!
                )
        )
    }
    
}