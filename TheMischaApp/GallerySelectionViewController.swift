import UIKit

class GallerySelectionViewController: UIViewController {
    
    var width: CGFloat = 0
    var height: CGFloat = 0
    
    var roadTripImage = UIImageView(
        image: UIImage(
            named: "New Mexico-6"
        )
    )
    
    var wonderfulWorldImage = UIImageView(
        image: UIImage(
            named: "UpClose-8"
        )
    )
    
    
    @IBAction func onSelectionTap(sender: UITapGestureRecognizer) {
        if sender.locationInView(self.view).y > self.view.frame.height/2{
            performSegueWithIdentifier("wonderfulWorldSegue", sender: self);
        }
        else {
            performSegueWithIdentifier("roadTripSegue", sender: self);
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        roadTripImage.frame = CGRectMake(CGFloat(0), CGFloat(0), self.view.frame.width, self.view.frame.height / 2)
        roadTripImage.contentMode = UIViewContentMode.Center
        roadTripImage.contentMode = UIViewContentMode.ScaleAspectFill
        roadTripImage.clipsToBounds = true
        
        var roadTripLabel: UILabel = UILabel(frame: roadTripImage.frame)
        roadTripLabel.text = "Road Trip Gallery"
        roadTripLabel.textAlignment = .Center
        roadTripLabel.textColor = UIColor.whiteColor()
        roadTripLabel.font = UIFont.systemFontOfSize(48)
        
        view.addSubview(roadTripImage)
        view.addSubview(roadTripLabel)
        
        wonderfulWorldImage.frame = CGRectMake(CGFloat(0), CGFloat(self.view.frame.height / 2), self.view.frame.width, self.view.frame.height / 2)
        wonderfulWorldImage.contentMode = UIViewContentMode.Center
        wonderfulWorldImage.contentMode = UIViewContentMode.ScaleAspectFill
        wonderfulWorldImage.clipsToBounds = true
        
        var wonderfulWorldLabel: UILabel = UILabel(frame: wonderfulWorldImage.frame)
        wonderfulWorldLabel.text = "Wonderful World Gallery"
        wonderfulWorldLabel.textAlignment = .Center
        wonderfulWorldLabel.textColor = UIColor.whiteColor()
        wonderfulWorldLabel.font = UIFont.systemFontOfSize(48)
        
        view.addSubview(wonderfulWorldImage)
        view.addSubview(wonderfulWorldLabel)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
