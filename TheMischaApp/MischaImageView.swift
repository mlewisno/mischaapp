import UIKit

class MischaImageView: UIView {
    
    let mischaImageManager = MischaTripImageManager.mischaManagerInstance

    override func drawRect(rect: CGRect)
    {
        super.drawRect(rect)
        
        let mischaImageIndex = Int(arc4random_uniform(UInt32(mischaImageManager.numberOfImages)))
                
        let mischaImageView = UIImageView(
            image: UIImage(
                named: MischaTripImageManager.mischaImageName(mischaImageIndex)
            )
        )
        
        mischaImageView.frame = CGRectMake(CGFloat(0), CGFloat(0), super.frame.width, super.frame.height)
        mischaImageView.contentMode = UIViewContentMode.Center
        mischaImageView.contentMode = UIViewContentMode.ScaleAspectFill
        mischaImageView.clipsToBounds = true
        
        self.addSubview(mischaImageView)
        
    }

}
