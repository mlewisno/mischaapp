import Foundation
import UIKit

private let sharedNatureManagerInstance = NatureImageManager()

class NatureImageManager {
    
    let imageSets = [
        ["ColumbiaRiver" : 21],
        ["UpClose" : 16],
    ]
    
    class var natureManagerInstance: NatureImageManager {
        return sharedNatureManagerInstance
    }
    
    class func natureImageNameWithGeneralIndex(imageIndex: Int) -> String {
        
        var index = imageIndex
        
        for (var i = 0; i < natureManagerInstance.imageSets.count; i++) {
            for (imageSet, number) in natureManagerInstance.imageSets[i] {
                if index < number {
                    return "\(imageSet)-\(index)"
                }
                else {
                    index -= number
                }
            }
        }
        return ""
    }
    
    class func getIndexForFirstImageInSelectedSection(name: String) -> Int {
        
        var imageCount = 0
        
        for (var i = 0; i < natureManagerInstance.imageSets.count; i++) {
            for (imageSet, number) in natureManagerInstance.imageSets[i] {
                if name == imageSet {
                    return imageCount
                }
                else {
                    imageCount += number
                }
            }
        }
        return 0
    }
    
    init() {
    }
    
}
