import UIKit
import QuartzCore
import MessageUI

class MessageCreationViewController: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var subjectInput: UITextField!
    @IBOutlet weak var namePromptLabel: UILabel!
    @IBOutlet weak var messageInput: UITextView!
    
    var myMail: MFMailComposeViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myMail = MFMailComposeViewController()

        myMail.mailComposeDelegate = self

        messageInput.layer.borderColor = UIColor.grayColor().CGColor
        messageInput.layer.borderWidth = 0.2
        messageInput.layer.cornerRadius = 5
        
    }
    
    @IBAction func onSendButtonTap(sender: UIButton) {
        if(MFMailComposeViewController.canSendMail()){
            myMail = MFMailComposeViewController()
            
            myMail.setSubject("[App Mail] - \(self.subjectInput.text)")
            
            var toRecipients = ["mlewisno@gmail.com"]
            myMail.setToRecipients(toRecipients)
            
            myMail.setMessageBody(self.messageInput.text, isHTML: true)
                        
            //Display the view controller
            self.presentViewController(myMail, animated: true, completion: nil)
        }
        else{
            var alert = UIAlertController(title: "Alert", message: "Your device cannot send emails", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mailComposeController(controller: MFMailComposeViewController!,
        didFinishWithResult result: MFMailComposeResult,
        error: NSError!){
            
            switch(result.value){
            case MFMailComposeResultSent.value:
                println("Email sent")
                
            default:
                println("Whoops")
            }
            
            self.dismissViewControllerAnimated(true, completion: nil)
            
    }

}
