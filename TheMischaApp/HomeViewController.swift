import UIKit
import MessageUI
import QuartzCore
import StoreKit

class HomeViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var mischaImageView: MischaImageView!
    @IBOutlet weak var scrollingImageView: ImageScrollView!
    @IBOutlet weak var aboutButtonView: AboutView!
    @IBOutlet weak var contactButtonView: ContactView!
    @IBOutlet weak var donationButtonView: DonationView!
    @IBOutlet weak var mischaPointsView: MischaBucksView!
    
    var inAppPurchaseManager = InAppPurchaseManager.inAppPurchaseManager
    
    var subviews: [UIView]?
    
    @IBAction func onAboutViewTap(sender: UITapGestureRecognizer) {
        let aboutMeViewController = storyboard!.instantiateViewControllerWithIdentifier("aboutMeViewController") as AboutMeViewController
        
        let aboutMePopover = UIPopoverController(contentViewController: aboutMeViewController)
        
        aboutMePopover.popoverContentSize = CGSize(width: 2 * super.view.frame.width / 3, height: 2 * super.view.frame.height / 3)
        aboutMePopover.contentViewController.view.frame = CGRectMake(CGFloat(0), CGFloat(0), 2 * super.view.frame.width / 3, 2 * super.view.frame.height / 3)
        aboutMePopover.presentPopoverFromRect(CGRectMake(0, 5 * super.view.frame.height / 6, super.view.frame.width, 0), inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Down, animated: true)
    }
    
    @IBAction func onContactViewTap(sender: UITapGestureRecognizer) {
        
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
        
    }
    
    @IBAction func onDonationTap(sender: UITapGestureRecognizer) {
        
        // Simulate the in app purchases
        let purchasesAlertController = UIAlertController(title: "Purchase Mischa Bucks", message: "Do you want to throw me a dollar and get Mischa Bucks!?", preferredStyle: UIAlertControllerStyle.Alert)
        
        let defaultAction = UIAlertAction(title: "Here's A Buck!", style: .Default, handler: { action in
            let mischaBucks = Int(arc4random_uniform(10) + 1)
            
            let oldBucks = NSUserDefaults.standardUserDefaults().valueForKey("numberOfMischaBucks") as Int
            NSUserDefaults.standardUserDefaults().setValue(oldBucks + mischaBucks, forKey: "numberOfMischaBucks")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            let thankYouAlertController = UIAlertController(title: "Thank You!", message: "You received \(mischaBucks) Mischa Bucks!", preferredStyle: UIAlertControllerStyle.Alert)
            let newCancelAction = UIAlertAction(
                title: "Cool!",
                style: UIAlertActionStyle.Default,
                handler: { junk in self.mischaPointsView.reload()}
            )
            thankYouAlertController.addAction(newCancelAction)
            
            self.presentViewController(thankYouAlertController, animated: true, completion: nil)
        })
        let cancelAction = UIAlertAction(title: "No Thanks!", style: .Cancel, handler: nil)
        
        purchasesAlertController.addAction(defaultAction)
        purchasesAlertController.addAction(cancelAction)
        
        presentViewController(purchasesAlertController, animated: true, completion: nil)
        
        inAppPurchaseManager.loadStore()
        if inAppPurchaseManager.canMakePurchases() {
            inAppPurchaseManager.purchaseMischaBucks()
        }
        
    }
    
    @IBAction func onMischaBucksTap(sender: UITapGestureRecognizer) {
        let currentBucks = NSUserDefaults.standardUserDefaults().valueForKey("numberOfMischaBucks") as Int
        
        if currentBucks > 0 {
            let rewardsViewController = storyboard!.instantiateViewControllerWithIdentifier("rewardScreenViewController") as UIViewController
            
            let rewardsPopover = UIPopoverController(contentViewController: rewardsViewController)
            
            rewardsPopover.popoverContentSize = CGSize(width: 2 * super.view.frame.width / 3, height: 2 * super.view.frame.height / 3)
            rewardsPopover.contentViewController.view.frame = CGRectMake(CGFloat(0), CGFloat(0), 2 * super.view.frame.width / 3, 2 * super.view.frame.height / 3)
            rewardsPopover.presentPopoverFromRect(CGRectMake(0, 5 * super.view.frame.height / 6, super.view.frame.width, 0), inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Down, animated: true)
        }
        else {
            let needMoreDoughActionController = UIAlertController(title: "Sorry!", message: "Sorry, you have 0 Mischa Bucks. Gain Mischa Bucks by donating!", preferredStyle: UIAlertControllerStyle.Alert)
            let newCancelAction = UIAlertAction(
                title: "Okay!",
                style: UIAlertActionStyle.Default,
                handler: nil)
            needMoreDoughActionController.addAction(newCancelAction)
            presentViewController(needMoreDoughActionController, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        subviews = [
            mischaImageView ,
            scrollingImageView ,
            aboutButtonView ,
            contactButtonView ,
            donationButtonView ,
            mischaPointsView
        ]
        
        for view in subviews! {
            view.alpha = 0.0
        }
        
        NSUserDefaults.standardUserDefaults().setValue(0, forKey: "numberOfMischaBucks")

    }
    
    override func viewDidAppear(animated: Bool) {
        fadeViewIn(0)
    }
    
    func fadeViewIn(index: Int){
        if (index < subviews!.count){
            if (index < 2) {
                UIView.animateWithDuration(0.75, delay: 0, options: nil, animations: {
                    self.subviews![index].alpha = 1.0
                    }, completion: { done in
                        self.fadeViewIn(index + 1)
                })
            }
            else {
                UIView.animateWithDuration(1.5, delay: 0, options: nil, animations: {
                    self.subviews![index].alpha = 1.0
                    }, completion: { done in
                        self.fadeViewIn(index + 1)
                })
            }
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "popoverMessageCreator"){
            let messageCreationViewController = segue.destinationViewController as? MessageCreationViewController
            messageCreationViewController?.view.frame = CGRectMake(
                0,
                0,
                self.view.frame.width / 2,
                self.view.frame.height / 1.25
            )
        }
    }
    
    func fadeScrollingImageView(){
        UIView.animateWithDuration(0.75, delay: 0, options: nil, animations: {
            self.scrollingImageView.alpha = 1.0
            }, completion: { done in
                self.fadeScrollingImageView()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        
        mailComposerVC.setToRecipients(["mlewisno@gmail.com"])
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

}
